# Use the official Golang image as a parent image
# https://hub.docker.com/_/golang
FROM golang:1.21

# Argument to pass the Golang path at build time
ARG GOLANG_PATH

# Set the user to root to perform installation and configuration
USER root

# Set environment variables for the HTTP service
ENV HTTP_SERVICE_HOME /usr/http-service
ENV HTTP_SERVICE_LOGS /var/log/http-service
ENV HTTP_SERVICE_USER http-svc
ENV HTTP_SERVICE_GROUP http-svc
ENV HTTP_SERVICE_ARTIFACT_NAME golang-service.go
ENV GOCACHE=/usr/http-service/.cache

# Set environment variables for the locale
ENV TZ America/Mexico_City
ENV LANG es_MX.UTF-8
ENV LANGUAGE es_MX.UTF-8
ENV LC_ALL es_MX.UTF-8

# Install dependencies and configure locale and user
RUN apt-get update \
	&& apt-get install -y --no-install-recommends tzdata locales curl \
	# Enable the specified locale
    && sed -i "s/^# *\($LANG\)/\1/" /etc/locale.gen \
	# Generate the locale
	&& locale-gen && update-locale LANG=${LANG} LANGUAGE=${LANGUAGE} LC_ALL=${LC_ALL} \
	# Create a new group and user for the HTTP service
	&& addgroup --system --gid 1000 ${HTTP_SERVICE_GROUP} && adduser --system --uid 1000 ${HTTP_SERVICE_USER} --ingroup ${HTTP_SERVICE_GROUP} \
	# Create directories for the HTTP service and set permissions
	&& mkdir -p ${HTTP_SERVICE_HOME} \
	&& chown -R ${HTTP_SERVICE_USER} ${HTTP_SERVICE_HOME} \
	&& chgrp -R ${HTTP_SERVICE_GROUP} ${HTTP_SERVICE_HOME} \
	&& mkdir -p ${HTTP_SERVICE_LOGS} \
	&& chown -R ${HTTP_SERVICE_USER} ${HTTP_SERVICE_LOGS} \
	&& chgrp -R ${HTTP_SERVICE_GROUP} ${HTTP_SERVICE_LOGS}

# Define the volume for logs
VOLUME ${HTTP_SERVICE_LOGS}

# Expose port for the service
EXPOSE 3333

# Set the working directory inside the container
WORKDIR ${HTTP_SERVICE_HOME}

# Switch to the non-root user for security
USER ${HTTP_SERVICE_USER}

# Copy the Golang application source code to the container
COPY ${GOLANG_PATH} ${HTTP_SERVICE_HOME}

# Command to run the application
ENTRYPOINT go run ${HTTP_SERVICE_HOME}/${HTTP_SERVICE_ARTIFACT_NAME}